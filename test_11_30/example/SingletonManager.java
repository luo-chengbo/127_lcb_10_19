package test_11_30.example;

import java.util.HashMap;
import java.util.Map;
public class SingletonManager {
    private static Map<String, Object> objMap =new HashMap<String, Object>() ;
    public static void regsiterService(String key, Object instance){
        if (!objMap.containsKey(key)){
            objMap.put(key, instance) ;
        }
    }
    public static Object getService(String key){
        return objMap .get (key) ;
    }
}
