package test_11_30.example;

public class Singeleton2 {
    private Singeleton2(){}

    public static Singeleton2 getInstance(){
        return Singeleton2Holder.instance;
    }
    // 静态内部类
    private static class Singleton2Holder{
        private static Singeleton2 instance = new Singeleton2();
    }
}
