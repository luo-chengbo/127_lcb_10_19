package test_11_30.example;
import java.util.ArrayList;

public class Thing implements Cloneable {
    // 定义一个私有变量
    private ArrayList<String> arrayList = new ArrayList<>();

    // 添加元素到arrayList
    public void addItem(String item) {
        arrayList.add(item);
    }

    // 测试方法
    public void printItems() {
        for (String item : arrayList) {
            System.out.println(item);
        }
    }

    @Override
    public Thing clone() {
        try {
            Thing thing = (Thing) super.clone();
            thing.arrayList = (ArrayList<String>) this.arrayList.clone();
            return thing;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

    // 测试示例
    public static void main(String[] args) {
        Thing original = new Thing();
        original.addItem("Item 1");
        original.addItem("Item 2");

        // 克隆原型对象
        Thing cloned = original.clone();

        // 修改克隆对象的值
        cloned.addItem("Item 3");

        // 打印原型对象和克隆对象的值
        System.out.println("Original:");
        original.printItems();
        System.out.println("Cloned:");
        cloned.printItems();
    }
}
