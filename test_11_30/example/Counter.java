package test_11_30.example;

public class Counter {
    private static Counter instance;
    private int onlineCount;
    private Counter(){
        System.out.println("在线人数统计器被创建了");
    }

    public static synchronized Counter getInstance(){
        if (instance==null){
            instance = new Counter();
        }
        return instance;
    }
    public synchronized void add() {
        onlineCount++;
    }
    public int getOnline() {
        return onlineCount;
    }

}

 class Test {
    public static void main(String[] args) {
        Counter counter = Counter.getInstance();
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                counter.add();
                System.out.println(counter.getOnline());
            }).start();
        }
    }
}