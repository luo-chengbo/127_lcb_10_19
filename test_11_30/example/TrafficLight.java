package test_11_30.example;

import java.util.HashMap;

public class TrafficLight {
    private static final String RED = "红";
    private static final String YELLOW = "黄";
    private static final String GREEN = "绿";
    private static HashMap<String, TrafficLight> trafficLightMap = new HashMap<>();

    private String color;


    //
    private TrafficLight(String color) {
        this.color = color;
    }

    public static synchronized TrafficLight getInstance(String color) {
        if (!trafficLightMap.containsKey(color)) {
            trafficLightMap.put(color, new TrafficLight(color));
        }
        return trafficLightMap.get(color);
    }

    public void light() {
        System.out.println("现在红绿灯的颜色是：" + color);
    }
}
 class TrafficLightTest {
    public static void main(String[] args) {
        TrafficLight red = TrafficLight.getInstance("红");
        TrafficLight yellow = TrafficLight.getInstance("黄");
        TrafficLight green = TrafficLight.getInstance("绿");

        red.light();
        yellow.light();
        green.light();

        TrafficLight sameRed = TrafficLight.getInstance("红");
        System.out.println(red == sameRed); // 输出 true
    }
}