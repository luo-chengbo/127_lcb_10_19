package test_10_26;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class TestClass {

    @Test
    public void testCharge() {
        OldPlan oldPlan = new OldPlan();
        oldPlan.setRegularRate(0.1f);
        oldPlan.setRegularServiceCharge(5.0f);
        oldPlan.setSummerRate(0.08f);

        Summer summer = new Summer(
                new Date(2023, 5, 1), // 6月1日
                new Date(2023, 7, 31) // 8月31日
        );

        Order order = new Order(100, new Date(), oldPlan, summer);

        float charge = order.charge();
        Assert.assertEquals(15.0f, charge, 0.01f);
    }
}