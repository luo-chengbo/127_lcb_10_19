package test_10_26;

import java.util.Date;

public class Order {
    private int quantity;
    private Date date;
    private OldPlan oldPlan;
    private Summer summer;

    public Order(int quantity, Date date, OldPlan oldPlan, Summer summer) {
        this.quantity = quantity;
        this.date = date;
        this.oldPlan = oldPlan;
        this.summer = summer;
    }

    public float charge() {
        float rate;
        if (summer.isSummer(date)) {
            rate = oldPlan.getSummerRate();
        } else {
            rate = oldPlan.getRegularRate();
        }

        float basicCharge = quantity * rate;
        float totalCharge = basicCharge + oldPlan.getRegularServiceCharge();

        return totalCharge;
    }
}
