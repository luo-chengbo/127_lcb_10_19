package test_10_26;

import java.util.Date;

public class OldPlan {
    private float summerRate;
    private float regularRate;
    private float regularServiceCharge;


    public float getSummerRate() {
        return summerRate;
    }

    public void setSummerRate(float summerRate) {
        this.summerRate = summerRate;
    }

    public float getRegularRate() {
        return regularRate;
    }

    public void setRegularRate(float regularRate) {
        this.regularRate = regularRate;
    }

    public float getRegularServiceCharge() {
        return regularServiceCharge;
    }

    public void setRegularServiceCharge(float regularServiceCharge) {
        this.regularServiceCharge = regularServiceCharge;
    }

}
