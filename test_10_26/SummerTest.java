package test_10_26;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class SummerTest {
    @Test
    public void testIsSummer() {
        Date start = new Date(2023, 5, 1); // 6月1日
        Date end = new Date(2023, 7, 31); // 8月31日

        Summer summer = new Summer(start, end);

        Date date = new Date(2023, 6, 15); // 7月15日
        boolean isSummer = summer.isSummer(date);

        Assert.assertTrue(isSummer);
    }
}
