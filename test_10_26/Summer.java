package test_10_26;

import java.util.Date;

public class Summer {
    private Date start;
    private Date end;

    public Summer(Date start, Date end) {
        this.start = start;
        this.end = end;
    }

    public boolean isSummer(Date date) {
        return date.after(start) && date.before(end);
    }
}
