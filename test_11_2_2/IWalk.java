package test_11_2_2;

public interface IWalk extends IPeoson {
    void drive();
    void setPeoson(IPeoson iPeoson);
}
