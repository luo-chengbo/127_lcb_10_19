package test_11_23.example.dome03;

public interface UserDao {
    public void insert();
    public void delete();
    public void update();
    public void select();
}
