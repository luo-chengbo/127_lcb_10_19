package test1;

import org.junit.Test;

import static org.junit.Assert.*;

public class VehicleTest {

    @Test
    public void testRunOnRoad() {
        Vehicle vehicle = new GroundVehicle();
        String expected = "火车在公路上运行";
        assertEquals(expected, vehicle.run("火车"));
    }

    @Test
    public void testRunInAir() {
        Vehicle vehicle = new AirVehicle();
        String expected = "飞机在空中上运行";
        assertEquals(expected, vehicle.run("飞机"));
    }

    @Test
    public void testRunOnWater() {
        Vehicle vehicle = new WaterVehicle();
        String expected = "轮船在水上上运行";
        assertEquals(expected, vehicle.run("轮船"));
    }
}
