public class sigleResponsibility {
    public static void main(String[] args) {
        Vehicle vehicle = new Vehicle();
        vehicle.run("火车");
        vehicle.runair("飞机");
        vehicle.runwater("轮船");
    }
}
class Vehicle{
    public Object run(String vehicle){
        System.out.println(vehicle+"在公路上运行");
        return null;
    }
    public void runair(String vehicle){
        System.out.println(vehicle+"在空中上运行");
    }
    public void runwater(String vehicle){
        System.out.println(vehicle+"在水上上运行");

    }
}