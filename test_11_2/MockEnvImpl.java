package test_11_2;

import java.time.LocalDateTime;

public class MockEnvImpl implements ISystemEnv{
    private LocalDateTime Now;


    @Override
    public void setDateTime(LocalDateTime time) {
        Now = time;
    }

    @Override
    public LocalDateTime getDatatime() {
        return Now;
    }
}
