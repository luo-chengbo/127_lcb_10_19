package test_11_2;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;

public class HelloWorldTest {
    @Test
    public void sayHelloGoodMoring(){

        ISystemEnv env = new SystemEnvImpl();
        HelloService service = new HelloService(env);
        String greet = service.sayHello("wangrong");
        Assert.assertEquals("上午好，wangrong", greet);
    }
    @Test
    public void sayHelloGoodAfternoon(){
        ISystemEnv env = new MockEnvImpl();
        HelloService service = new HelloService(env);
        env.setDateTime(LocalDateTime.of(2023,11,02,16,15,0,0));
        String greet = service.sayHello("wangrong");
        Assert.assertEquals("下午好，wangrong", greet);
    }

    @Test
    public void playSoundTest(){
        ISystemEnv env = new SystemEnvImpl();
        HelloService service = new HelloService();
        String greet = service.playSound("汪汪汪");
        Assert.assertEquals("发出：汪汪汪",greet);
    }

}
