package test_11_2;

public class HelloService {
    private ISystemEnv env;
    public HelloService(ISystemEnv myenv){
        env = myenv;
    }
    public HelloService(){

    }
    String sayHello(String username){
        HelloWorld helloworld = new HelloWorld();
        return helloworld.sayHello(username, env.getDatatime());
    }
    String playSound(String sound){
        HelloWorld helloworld = new HelloWorld();
        return helloworld.playSound(sound);
    }
}
