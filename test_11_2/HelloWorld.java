package test_11_2;

import java.time.LocalDateTime;

public class HelloWorld {
    String sayHello(String name, LocalDateTime datatime){

        int hour = datatime.getHour();
        if(hour<=12) return ("上午好，" + name);
        return ("下午好，" + name);
    }
    String playSound(String sound){
        return ("发出：" + sound);
    }
}
