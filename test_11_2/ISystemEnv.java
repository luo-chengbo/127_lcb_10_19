package test_11_2;

import java.time.LocalDateTime;

public interface ISystemEnv {
    public void setDateTime(LocalDateTime time);
    public LocalDateTime getDatatime();
}
