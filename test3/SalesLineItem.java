package test3;

public class SalesLineItem {

    private String quantity;

    public double subtotal(){
        return getProductSpecification().getPrice();
    }

    public ProductSpecification getProductSpecification(){
        return new ProductSpecification();
    }
}
