package test3;

import java.util.List;

public class Sale {
    private String date;
    private String time;
    private List<SalesLineItem> lineItems;
    public double total(){
        double totalPrice=0;
        for (SalesLineItem item:lineItems ){
            totalPrice+=item.subtotal();
        }
        return totalPrice;
    }

}


