package test_12_7.example;

import java.util.Date;

public class YearMonth {
    private int year;
    private int month;
    public YearMonth(String ym) {
        year = Integer.parseInt(ym.substring(0, 4));
        month = Integer.parseInt(ym.substring(4, 6));
    }

    public YearMonth(int newyear, int newmonth) {
        year = newyear;
        month = newmonth;
    }
    public YearMonth(Date date) {
        year = date.getYear();
        month = date.getMonth();
    }

    public int getYear() {
        return year;
    }


    public int getMonth() {
        return month;
    }

    public YearMonth previous() {
        if(month==1){
            year = year-1;
            month= 12;
        }else
        {
            year = year;
            month = month-1;
        }
        return new YearMonth(year , month);
    }

    public YearMonth next() {
        if(month==12){
            year = year+1;
            month= 1;
        }else
        {
            year = year;
            month = month+1;
        }
        return new YearMonth(year , month);
    }
}

