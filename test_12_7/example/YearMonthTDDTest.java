package test_12_7.example;


import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class YearMonthTDDTest {
    @Test
    public void CreateYearMonthbyString(){
        YearMonth yearMonth = new YearMonth("200306");
        Assert.assertEquals(2003, yearMonth.getYear());
        Assert.assertEquals(6, yearMonth.getMonth());
    }

    @Test
    public void CreateYearMonthbyString2(){
        YearMonth yearMonth = new YearMonth("200205");
        Assert.assertEquals(2002, yearMonth.getYear());
        Assert.assertEquals(5, yearMonth.getMonth());
    }

    @Test
    public void CreateYearMonthbyInt(){
        YearMonth yearMonth = new YearMonth(2003, 6);
        Assert.assertEquals(2003, yearMonth.getYear());
        Assert.assertEquals(6, yearMonth.getMonth());
    }

    @Test
    public void CreateYearMonthbyDate(){
        YearMonth yearMonth = new YearMonth(new Date(2003, 6, 7));
        Assert.assertEquals(2003, yearMonth.getYear());
        Assert.assertEquals(6, yearMonth.getMonth());
    }

    @Test
    public void PreviousYearMonthbyDate(){
        YearMonth yearMonth = new YearMonth(2003, 1);
        YearMonth previousYearMonth = yearMonth.previous();
        Assert.assertEquals(2002, yearMonth.getYear());
        Assert.assertEquals(12, previousYearMonth.getMonth());
    }

    @Test
    public void NextYearMonthby1(){
        YearMonth yearMonth = new YearMonth(2003, 6);
        YearMonth previousYearMonth = yearMonth.next();
        Assert.assertEquals(2003, yearMonth.getYear());
        Assert.assertEquals(7, previousYearMonth.getMonth());
    }

    @Test
    public void NextYearMonthby2(){
        YearMonth yearMonth = new YearMonth(2003, 12);
        YearMonth previousYearMonth = yearMonth.next();
        Assert.assertEquals(2004, yearMonth.getYear());
        Assert.assertEquals(1, previousYearMonth.getMonth());
    }

}

