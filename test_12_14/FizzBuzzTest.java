package test_12_14;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FizzBuzzTest {

    @Test
    public void should_return_Fizz_when_given_3(){
        FizzBuzz fizzBuzz = new FizzBuzz(3);
        String result = fizzBuzz.fizzBuzz();
        assertEquals("Fizz",result);
    }

    @Test
    public void should_return_Buzz_when_given_5(){
        FizzBuzz fizzBuzz = new FizzBuzz(5);
        String result = fizzBuzz.fizzBuzz();
        assertEquals("Buzz",result);
    }

    @Test
    public void should_return_FizzBuzz_when_given_15(){
        FizzBuzz fizzBuzz = new FizzBuzz(15);
        String result = fizzBuzz.fizzBuzz();
        assertEquals("FizzBuzz",result);
    }

    @Test
    public void should_return_1_when_given_1(){
        FizzBuzz fizzBuzz = new FizzBuzz(1);
        String result = fizzBuzz.fizzBuzz();
        assertEquals("1",result);
    }

    @Test (expected = IllegalAccessError.class)
    public void should_return_IllegalAccessError_when_given_101(){
        FizzBuzz fizzBuzz = new FizzBuzz(101);
    }

    @Test (expected = IllegalAccessError.class)
    public void should_return_IllegalAccessError_when_given_0(){
        FizzBuzz fizzBuzz = new FizzBuzz(0);
    }

    @Test
    public void should_return_Fizz_when_given_13(){
        FizzBuzz fizzBuzz = new FizzBuzz(13);
        String result = fizzBuzz.fizzBuzz();
        assertEquals("Fizz",result);
    }

    @Test
    public void should_return_Fizz_when_given_52(){
        FizzBuzz fizzBuzz = new FizzBuzz(52);
        String result = fizzBuzz.fizzBuzz();
        assertEquals("Buzz",result);
    }

    @Test
    public void should_return_Fizz_when_given_35(){
        FizzBuzz fizzBuzz = new FizzBuzz(35);
        String result = fizzBuzz.fizzBuzz();
        assertEquals("FizzBuzz",result);
    }

    @Test
    public void should_return_Fizz_when_given_51(){
        FizzBuzz fizzBuzz = new FizzBuzz(51);
        String result = fizzBuzz.fizzBuzz();
        assertEquals("FizzBuzz",result);
    }
}