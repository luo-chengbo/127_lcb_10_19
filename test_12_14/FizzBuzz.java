package test_12_14;

public class FizzBuzz {
    private int Num;
    public FizzBuzz(int num) {
        if(num>100 || num == 0) throw new IllegalAccessError();
        Num = num;
    }

    public String fizzBuzz() {
        String strNum = String.valueOf(Num);
        if (Num % 3 == 0 && Num % 5 == 0 || strNum.contains("3") && strNum.contains("5") || Num % 3 == 0 && strNum.contains("5")) return "FizzBuzz";
        if (Num % 5 == 0 || strNum.contains("5")) return "Buzz";
        if (Num % 3 == 0 || strNum.contains("3")) return "Fizz";

//        if(strNum.contains("3") && strNum.contains("5")) return "FizzBuzz";
//        if(strNum.contains("3")) return "Fizz";
//        if(strNum.contains("5")) return "Buzz";


        return String.valueOf(Num);
    }
}
